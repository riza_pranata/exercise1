
let reject=require('./posibilityOption/reject')
let replace=require('./posibilityOption/replace')
let stack=require('./posibilityOption/stack')
let reverse=require('./posibilityOption/reverse')


const array=[['Yellow','Yellow','Red'],['Yellow','Red'],['Red'],['Red'],['Yellow','Red'],['Yellow','Yellow','Red']]
const secondArray=[['Red','Red'],['Red','Yellow'],['Cyan','Yellow'],['Cyan','Cyan']]
const thirdArray=[['Brown'],['Orange'],['Orange'],['Yellow'],['Yellow'],['Yellow'],['Orange'],['Orange'],['Brown']]

test('first program : replace1to1',()=>{
    expect(replace.replace(array,'Yellow','Red')).toEqual(
        [['Red','Red','Red'],['Red','Red'],['Red'],['Red'],['Red','Red'],['Red','Red','Red']]
    )
})


test('test function reject',()=>{
    expect(reject.reject(array,'Yellow')).toEqual(
        [['Red'],['Red'],['Red'],['Red'],['Red'],['Red']]
    )
})

test('test function stack',()=>{
    expect(stack.stack(array,'Yellow')).toEqual(
        [['Yellow','Yellow','Red','Yellow'],['Yellow','Red','Yellow'],['Red','Yellow'],['Red','Yellow'],['Yellow','Red','Yellow'],['Yellow','Yellow','Red','Yellow']]
    )
})

test('test function reverse',()=>{
    expect(reverse.reverse(secondArray)).toEqual(
        [['Red','Red'],['Yellow','Red'],['Yellow','Cyan'],['Cyan','Cyan']]
    )
})
