const replace=(array,oldColor,newColour)=>{
    return array.map(element=>{
        return element.map(el=>{
            if(el==oldColor){
                return newColour
            } else{
                return el
            }
        })
    })
}

module.exports={replace}