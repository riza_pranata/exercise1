const reject=(array,rejectedColour)=>{
    return array.map(element=>{
        let temp=[]
        element.forEach(elem => {
            if(elem!=rejectedColour){
                temp.push(elem)
            }
        });
        return temp
    })
}

module.exports={reject}