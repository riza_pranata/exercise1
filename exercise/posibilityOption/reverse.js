const reverse=(array)=>{
    return array.map(element=>element.reverse())
}

module.exports={reverse}