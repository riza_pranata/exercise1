const stack=(array,colour)=>{
    return array.map(element=>{
        let temp=[]
        element.forEach((elem,index,array) => {
            temp.push(elem)
            if(index==array.length-1){
                temp.push(colour)
            }
        });
        return temp
    })
}

module.exports = {stack}
